// Uncomment following to enable zipkin tracing, tailor to fit your network configuration:
// const appzip = require('appmetrics-zipkin')({
//     host: 'localhost',
//     port: 9411,
//     serviceName:'frontend'
// });

require('appmetrics-dash').attach();
require('appmetrics-prometheus').attach();
const appName = require('./../package').name;
const http = require('http');
const express = require('express');
const log4js = require('log4js');
const localConfig = require('./config/local.json');
const path = require('path');

const logger = log4js.getLogger(appName);
const app = express();
const server = http.createServer(app);

app.use(log4js.connectLogger(logger, {
  level: process.env.LOG_LEVEL || 'info'
}));
const serviceManager = require('./services/service-manager');
require('./services/index')(app);
require('./routers/index')(app, server);

const arduino = require("johnny-five");
const board = new arduino.Board();

let red;
let green;
let blue;
 
board.on ('ready', () => {
  red = new arduino.Led (11);
  green = new arduino.Led (10);
  blue = new arduino.Led (9);
});

const bodyParser = require('body-parser');
// is this needed .... yes absolutely needed
// to get constiables from views form to app.js
// if needed , it has to be before app.router
//app.use(express.urlencoded());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// Add your code here
// const prompt = require('prompt-sync')();

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

function up (color) {
	switch (color) {
		case 'rojo':
			red.on ();
			break;
		case 'verde':
			green.on ();
			break;
		case 'azul':
			blue.on ();
			break;
	}
}

function blink (color) {
  switch (color) {
		case 'rojo':
			red.blink (500);
			break;
		case 'verde':
			green.blink (500);
			break;
		case 'azul':
			blue.blink (500);
			break;
	}
} 

function down (color) {
	switch (color) {
    case 'rojo':
      red.stop ();
			red.off ();
			break;
    case 'verde':
      green.stop ();
			green.off ();
			break;
    case 'azul':
      blue.stop ();
			blue.off ();
			break;
	}
}

function setOne (color, value) {
	switch (color) {
		case 'rojo':
      red.brightness (value);
			break;
		case 'verde':
      green.brightness (value);
			break;
		case 'azul':
      blue.brightness (value);
			break;
	}
}

function hexToRgb (hex) {
	// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
	const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
	hex = hex.replace(shorthandRegex, (m, r, g, b) => r + r + g + g + b + b);

  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  red.brightness (parseInt(result[1], 16));
  green.brightness (parseInt(result[2], 16));
  blue.brightness (parseInt(result[3], 16));
}

const watson = require('watson-developer-cloud');

const assistant = new watson.AssistantV1({
  iam_apikey: 'vbvzkI1AyHkLJ8Zy7HluySR6lFSajy9RQML3vMYIr0Cs',
  version: '2018-09-20'
});

app.post('/converse', (req, res) => {

  assistant.message({
    workspace_id: 'fe8c3886-eec3-4cd4-8069-c086f0fe717f',
    input: {
      'text': req.body.dialog
    },
    context: req.body.context
  }, (error, response) => {
    if (error) {
      console.log(error);
      return;
    }
    const data = response;
    
    if (data.actions) {
      const action = data.actions[0];
    
      const name = action.name;
    
    
      if (action && name) {
        if (name === 'up') {
          const led = action.parameters.colors;
          up (led);
        } else if (name === 'down') {
          const led = action.parameters.colors;
          down (led);
        } else if (name === 'set') {
          const hex = action.parameters.hex;
          hexToRgb(hex);
        } else if (name === 'setOne') {
          const { color, value } = action.parameters;
          setOne (color, value);
        } else if (name === 'blink') {
          const { color } = action.parameters;
          blink (color);
        } else if (name === 'stopBlink') {
          const { color } = action.parameters;
          down (color);
        }
      }
    }
    res.json(response)
  });
});

app.post('/synthesize', (req, res) => {
  console.log (req.body);
  var TextToSpeechV1 = require('watson-developer-cloud/text-to-speech/v1');
  var fs = require('fs');
  
  var textToSpeech = new TextToSpeechV1({
    username: '44bc9037-b6af-4688-b4cd-ba24b57722c8',
    password: 'wXa1quxjrQJS'
  });
  
  var synthesizeParams = {
    text: req.body.text,
    accept: 'audio/ogg',
    voice: 'es-LA_SofiaVoice'
  };
  
  // Pipe the synthesized text to a file.
  textToSpeech.synthesize(synthesizeParams).on('error', function(error) {
    console.log(error);
  }).pipe(res);
});

app.post ('/transcribe', (req, res) => {

  var SpeechToTextV1 = require('watson-developer-cloud/speech-to-text/v1');
  var fs = require('fs');
  var CombinedStream = require('combined-stream');
  
  var speechToText = new SpeechToTextV1({
    username: 'd735251f-8673-4b84-bf40-f9b36f8d2d4f',
    password: 'I2eqCRJhDMpT'
  });

  // create buffer object from base64 encoded string, it is important to tell the constructor that the string is base64 encoded
  const bitmap = new Buffer.from (req.body.audio.replace ('data:audio/ogg;base64,', ''), 'base64');
  // write buffer to file
  fs.writeFileSync('audio.ogg', bitmap);
  var combinedStream = CombinedStream.create();
  combinedStream.append(fs.createReadStream('audio.ogg'));

  var recognizeParams = {
      audio:  combinedStream,
      'content_type': 'audio/ogg',
      timestamps: true,
      'word_alternatives_threshold': 0.9,
      keywords: ['prende', 'foco', 'apaga', 'rojo', 'verde', 'azul'],
      'keywords_threshold': 0.5,
      model: 'es-ES_BroadbandModel'
  };

  console.log (recognizeParams);

  speechToText.recognize(recognizeParams, function(error, speechRecognitionResults) {
      if (error) {
          console.log(error);
      } else {
          console.log (speechRecognitionResults);
          res.json (speechRecognitionResults);
      }
  });    
});

const port = process.env.PORT || localConfig.port;
server.listen(port, function () {
  logger.info(`openthedoorhal listening on http://localhost:${port}/appmetrics-dash`);
  logger.info(`openthedoorhal listening on http://localhost:${port}`);
});

app.use(function (req, res, next) {
  res.sendFile(path.join(__dirname, '../public', '404.html'));
});

app.use(function (err, req, res, next) {
  res.sendFile(path.join(__dirname, '../public', '500.html'));
});
module.exports = server;